import React from 'react'
import {FlatList, View, TouchableOpacity, Text, Image, StyleSheet} from 'react-native'
import store from '../store'

const getUri = (id) => `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`

// ice: use gitlab repo
export default Pokemon = ({identifier, id, onPress}) => {
    
    return (
        <TouchableOpacity style={{
            alignItems:'center',
            justifyContent:'center',
            width:100,
            height:100
        }}
            onPress={onPress}
        >
            <View>
                <Image style={styles.image} source={{uri:getUri(id)}}/>
                <Text style={{
                        flex:1,
                        marginTop:5,
                        fontWeight:'bold'
                    }}>
                    {identifier}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    image:{
        width:64,
        height:64
    }
})