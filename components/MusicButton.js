import {Icon, colors} from 'react-native-elements'
import React from 'react'
import {TouchableOpacity} from 'react-native'
import store from '../store';

export default class MusicButton extends React.Component {
    constructor(){
        super()
        this.state = {musicPlaying: store.musicPlaying};
        this._listen1 = store.on("musicPlaying", (musicPlaying)=> this.setState({musicPlaying}));
    }
    // add unMount??

    render(){
        return(
            <TouchableOpacity onPress={store.toggleMusic}>
                <Icon 
                    name={this.state.musicPlaying ? "music": "music-off"} 
                    type="material-community" 
                    color={colors.primary2}
                    raised />
            </TouchableOpacity>
        )
    }

}