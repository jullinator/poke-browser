import EventStore from './event-store'
import Expo from 'expo'

class Store extends EventStore{

    musicPlaying = false;
    pokemons = [];
    music = new Expo.Audio.Sound();
    fontLoaded = false;


    loadFont = async ()=> {
        // When using custom fonts: DO NOT ADD FONT-WEIGHT (will not show up), use custom font for weight instead
        await Expo.Font.loadAsync("pokemon", require("./assets/fonts/pokemon.ttf"))
        this.update("fontLoaded", true);
    }
    loadAudio = async ()=> {
        await this.music.loadAsync(require('./assets/music.mp3'))
        await this.music.setIsLoopingAsync(true);
    }

    toggleMusic = async () =>{
        try{
            if(this.musicPlaying){
                var status = await this.music.pauseAsync();
                // unnecessary// if(!status.isPlaying)
            } else {
                var status = await this.music.playAsync();
            }
            this.update("musicPlaying", !this.musicPlaying);
        } catch (error){
            console.log(error);
        }
    }


    getUri = (id) => `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`

}

const store = new Store("musicPlaying", "fontLoaded");
store.pokemons = require('./pokemon.json');
store.loadFont();
store.loadAudio();

export default store;




