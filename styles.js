import {StyleSheet} from 'react-native'

export const colors = {
    red: '#FF003B'
}

export default styles = StyleSheet.create({
    list:{
        flexDirection: 'row',
        flexWrap:'wrap',
        alignItems:'flex-start',
        justifyContent:'space-around'
    },
    image:{
        width:64,
        height:64
    }
})

export const text = StyleSheet.create({
    normal:{
        fontSize:18
    }
})