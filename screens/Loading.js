import React from 'react'
import {Image, View, Dimensions} from 'react-native'
import {NavigationActions} from 'react-navigation'
import store from '../store';


export default class Loading extends React.Component {
    constructor(){
        super()
        this._listen1 = store.on("fontLoaded", (fontLoaded)=> {
            this.props.navigation.dispatch(NavigationActions.reset({
                index:0,
                actions:[
                    NavigationActions.navigate({routeName:"Browser"})
                ]
            }));
        })
    }
    componentWillUnmount(){
        this._listen1.off()
    }
    render(){
        return(
            <View style={{}}>
                <Image 
                    source={require('../assets/images/bg.jpg')} 
                    style={{width:Dimensions.get("window").width, height: Dimensions.get("window").height}}
                />
            </View>
        )
    }
}