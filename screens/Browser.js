import React from 'react'
import {FlatList, View, Text, StyleSheet, Dimensions} from 'react-native'
import store from '../store'
import PokemonItem from '../components/PokemonItem'


export default class Browser extends React.Component{


    _renderPokemon = ({item}) =>{
        const onPress = ()=>{
            const {navigate} = this.props.navigation
            navigate("Details", {
                pokemonID: item.id
            })
        }
        return <PokemonItem  {...item}
                             onPress = {onPress}
                />
    }

    render(){
        return (
            <FlatList numColumns={3}
                      horizontal={false}
                      contentContainerStyle={{
                        marginLeft: Dimensions.get("window").width*0.05,
                      }}
                      renderItem={this._renderPokemon}
                      data = {store.pokemons}
                      keyExtractor = {(item, index) => item.id}
                    />

        )
    }
}

const styles = StyleSheet.create({

})