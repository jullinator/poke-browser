import React from 'react'
import {View, Text, ActivityIndicator, Image, ScrollView, AsyncStorage} from 'react-native'
import store from '../store';
import styles, {text} from '../styles';
import { colors } from 'react-native-elements';

const Stat = ({label, value}) => {
    return (
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <Text style = {[text.normal, {fontWeight:"bold",marginLeft:25 }] } >{label}: </Text>
            <Text style= {[text.normal,{marginRight:25}]}>{value}</Text>
        </View>
    )
}
const labels = [
    "base_experience",
    "height",
    "weight",
]

export default class Details extends React.Component {
    
    data = {}
    state = {
        loaded:false
    }



    componentDidMount(){
        this.getData()
    }

    getData = async ()=>{
        try{
            const {pokemonID} = this.props.navigation.state.params;
            // Check if we have cached the result for this fucker
            var stored = await AsyncStorage.getItem(`${pokemonID}`);
            let data;
            if(stored==null){
                var res = await fetch(`http://pokeapi.co/api/v2/pokemon/${pokemonID}`)
                data = await res.json()
                var storeRes = await AsyncStorage.setItem(`${pokemonID}`, JSON.stringify(data));
            } else {
                data = JSON.parse(stored)
            }
            this.data = data
            // Cache the result, for later
            this.setState({loaded:true})
    
        } catch(error){
            console.log(error)
        }

    }

    renderLoading = () =>{
        return <ActivityIndicator size="large" />
    }

    renderPokemon = () =>{
        return (
            <ScrollView style={{

            }}>
                <View style={{flexDirection:'row', justifyContent:'space-around', backgroundColor:colors.primary2}}>
                    <View style={{flexDirection:'column', justifyContent:'center'}}>    
                        <Text style={[text.normal, {fontSize:22, fontFamily:'pokemon', color:'yellow', textShadowColor:'black', textShadowRadius:1, textShadowOffset:{width:0, height:3}}]}>
                            {this.data.name}
                        </Text>
                        <Text>ID: {this.data.id}</Text>
                    </View>
                    <View style={{}}>
                        <Image source={{uri:store.getUri(this.data.id)}} style={{height: 100, width:100}}/>
                    </View>
                </View>
                <View style={{marginTop:5, marginRight:5, marginLeft:5, borderRadius:5, borderColor:colors.grey2, borderWidth:1}}>
                    <Stat label="Base Experience" value={this.data.base_experience} />
                    <Stat label="Height" value = {this.data.height} />
                    <Stat label="Weight" value = {this.data.weight} />
                </View>
            </ScrollView>
        )
    }
    
    render(){
        let Component;
        if(this.state.loaded) Component = this.renderPokemon
        else                  Component = this.renderLoading
        return <Component />
    }
}