import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Expo from 'expo'
import {StackNavigator} from 'react-navigation'
import Browser from './screens/Browser'
import Details from './screens/Details'
import store from './store';
import Loading from './screens/Loading';
import MusicButton from './components/MusicButton'




console.disableYellowBox = true;
const Navigator = StackNavigator({
  Loading: {
    screen: Loading 

  },
  Browser:{
    screen: Browser,
    navigationOptions:({navigation}) => ({
      title:"Browser",
      headerRight:
        <View style={{flexDirection:'row'}}>
          <MusicButton />
        </View> 

    })
  },
  Details:{
    screen: Details,
    navigationOptions: ({navigation}) => ({
      title:store.pokemons.find( ({id})=> id == navigation.state.params.pokemonID).identifier,
    })

  }
})

export default ()=> <Navigator />
