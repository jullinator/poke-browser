import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Row extends React.Component{
    render(){
        const {
            style,
            ...attributes
        } = this.props;
        return(
            <View {...attributes} style={[style, {
                
            }]} >
                {this.props.children}
            </View>
        )
    }
}

